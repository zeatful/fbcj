import { Component, OnInit } from '@angular/core';
import { STAFF_MEMBERS } from './staff-list';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  public staffMembers = STAFF_MEMBERS;
  constructor() { }

  ngOnInit() {
  }

}