export interface Staff {
    name: string,
    title: string,
    phone: string
    email: string,
    about: string,
    image_url: string
}