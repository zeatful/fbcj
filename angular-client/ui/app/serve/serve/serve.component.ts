import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-serve',
  templateUrl: './serve.component.html',
  styleUrls: ['./serve.component.css']
})
export class ServeComponent implements OnInit {

  private opportunities = [
    { name: 'Kitchen Team', leader: 'Lynn Collins', contact: ''},
    { name: 'Setup Team', leader: 'Lynn Collins', contact: ''},
    { name: 'Church Security', leader: 'David Schoch', contact: ''},
    { name: 'Initial Contact Team', leader: ''},
    { name: 'Nursery Workers', leader: 'Fran Luttrell', contact: ''},
    { name: 'Sunday School Workers', leader: 'Bobby Gatchel', contact: ''},
    { name: 'Media Team', leader: 'Billy Wheeler', contact: ''}
  ];

  constructor() { }

  ngOnInit() {
  }

}
