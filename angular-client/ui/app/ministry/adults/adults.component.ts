import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adults',
  templateUrl: './adults.component.html',
  styleUrls: ['./adults.component.css']
})
export class AdultsComponent implements OnInit {

  private groups = [
    {
      name: "Young Adults",
      teacher: "Billy Taylor",
      location: "Room 211",
      description: "All Young Adults",
      summary: ""
    },
    {
      name: 'Adult Coed 1',
      teacher: 'Jerry Gibson & Calvin Rowell',
      location: 'Sanctuary',
      description: "--",
      summary: ""
    },
    { 
      name: "Adult Coed 2", 
      teacher: "Micah Fahner", 
      location: "Room 212", 
      description: "~40-55", 
      summary: "" 
    },
    {
      name: "Adult Coed 3", 
      teacher: "Don Luttrell & Mike Schwartz", 
      location: "Room 103", 
      description: "~55+", 
      summary: ""
    },
    { 
      name: "Men's Class", 
      teacher: "Ray Ives & Leroy Rudd", 
      location: "Room 100", 
      description: "Men", 
      summary: "" 
    },
    { 
      name: "Class of Ruth", 
      teacher: "Barbara Ard & Sylvia Widener", 
      location: "Room 102", 
      description: "Single Women", 
      summary: "We are a group of ladies from age 50 to whatever, come join us!" 
    },
    { 
      name: "Class of Faithful", 
      teacher: "Amanda Ambrose & Marie Metts", 
      location: "Room 104", 
      description: "Older Single Women", 
      summary: "Come enjoy gathering together, sharing our years of combined wisdom, sharing precious memories of times past, hearing old time sayings, and learning life lessons from one another" 
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}