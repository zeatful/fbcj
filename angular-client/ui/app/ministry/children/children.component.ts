import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit {

  private groups = [
    {
      name: "Nursery",
      teacher: "Volunteers",
      location: "BHS1",
      description: "<2yrs",
      summary: ""
    },
    {
      name: "Preschool",
      teacher: "Volunteers",
      location: "BHS2",
      description: "2-3yrs",
      summary: ""
    },
    {
      name: "1st - 2nd Grade",
      teacher: "Volunteers",
      location: "BHS5",
      description: "",
      summary: ""
    },
    {
      name: "3rd - 4th Grade",
      teacher: "Volunteers",
      location: "BHS4",
      description: "",
      summary: ""
    },
    {
      name: "5th - 6th Grade",
      teacher: "BJ Johnson & Judy Famorca",
      location: "BHS4",
      description: "",
      summary: ""
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
