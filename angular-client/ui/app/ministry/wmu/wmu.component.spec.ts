import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WmuComponent } from './wmu.component';

describe('WmuComponent', () => {
  let component: WmuComponent;
  let fixture: ComponentFixture<WmuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WmuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WmuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
