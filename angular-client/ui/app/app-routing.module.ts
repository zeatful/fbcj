import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { StaffComponent } from './about/staff/staff.component';
import { BeliefComponent } from './about/belief/belief.component';
import { ChildrenComponent } from './ministry/children/children.component';
import { YouthComponent } from './ministry/youth/youth.component';
import { StartComponent } from './start/start.component';
import { EventComponent } from './event/event.component';
import { CalendarComponent } from './calendar/calendar.component';
import { StreamComponent } from './stream/stream.component';
import { AdultsComponent } from './ministry/adults/adults.component';
import { ServeComponent } from './serve/serve/serve.component';
import { WorshipComponent } from './worship/worship.component';

const routes: Routes = [
  { path: '', component: LandingComponent},
  { path: 'start', component: StartComponent},
  { path: 'events', component: EventComponent},
  { path: 'calendar', component: CalendarComponent},
  { path: 'watch', component: StreamComponent},
  { path: 'staff', component: StaffComponent},
  { path: 'beliefs', component: BeliefComponent},
  { path: 'children', component: ChildrenComponent},
  { path: 'youth', component: YouthComponent},
  { path: 'children', component: ChildrenComponent},
  { path: 'adult', component: AdultsComponent},
  { path: 'serve', component: ServeComponent},
  { path: 'worship', component: WorshipComponent},
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
