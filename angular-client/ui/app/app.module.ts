import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './landing/landing.component';
import { StaffComponent } from './about/staff/staff.component';
import { BeliefComponent } from './about/belief/belief.component';
import { ChildrenComponent } from './ministry/children/children.component';
import { YouthComponent } from './ministry/youth/youth.component';
import { StartComponent } from './start/start.component';
import { EventComponent } from './event/event.component';
import { CalendarComponent } from './calendar/calendar.component';
import { StreamComponent } from './stream/stream.component';
import { AdultsComponent } from './ministry/adults/adults.component';
import { BrotherhoodComponent } from './ministry/brotherhood/brotherhood.component';
import { WmuComponent } from './ministry/wmu/wmu.component';
import { ServeComponent } from './serve/serve/serve.component';
import { WorshipComponent } from './worship/worship.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LandingComponent,
    StaffComponent,
    BeliefComponent,
    ChildrenComponent,
    YouthComponent,
    StartComponent,
    EventComponent,
    CalendarComponent,
    StreamComponent,
    AdultsComponent,
    BrotherhoodComponent,
    WmuComponent,
    ServeComponent,
    WorshipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
